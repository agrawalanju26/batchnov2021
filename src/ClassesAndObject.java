
public class ClassesAndObject {

	public void evenOrOdd(int i) {
		if(i%2==0) {
			System.out.println("number is even");
		}else {
			System.out.println("odd number");
		}
	}
	
	public int sum (int i,int j) {
//		System.out.println(i+j);
		int temp=i+j;
		return temp;
	}
	
	// private
	// Krishna
	// String
	// int , float
	
	private String krishna(int i,float f) {
		return null;
	}
	
	//protected 
	// float
	//anjali
	//double,int ,float
	
	protected float anjali(double i,int j, float k) {
		return 0.0f;
	}
	
	// ritu
	// non return 
	// no params
	
	
	void ritu() {
		
	}
	
	
	// public 
	// char
	//Yash
	// String  : -arg
	public char yash(String s) {
		return 'c';
	}
	
	
	//public 
	// return object 
	// ks
	// object
	
	public ClassesAndObject ks(ClassesAndObject obj1) {
		return null;
	}
	
	
	public void show() {
		System.out.println("rounak");
	}
	
	
	public static void main(String[] args) {

		ClassesAndObject obj = new ClassesAndObject();

//		obj.evenOrOdd(10);
//		obj.evenOrOdd(23);
		int i =obj.sum(12, 34);
		System.out.println(i);
		obj.evenOrOdd(i);

	}

}
