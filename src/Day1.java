
public class Day1 {

	public static void main(String[] args) {

		// Variables in Java


	

		// 1. Primitive

		boolean b = true; // 1 bit
		char c = 'a'; // 2 byte

		byte by = 12; // 1 byte -128 to 127
		short s = 23; // 2 byte -32768 to 32767
		int i = 34; // 4 byte -2^31 to 2^31-1
		long l = 56; // 8 byte -2^63 to 2^63-1

		float f = 12.32f; // 4 byte
		double d = 13.32;// 8 byte

		d = f;
		s = by;
//		System.out.println(s);

		// Type conversion
		// 1. implicit
		// 2. explicit
		by=(byte) s;
//		f=i;
		i=(int) f;
		System.out.println(i);
//		System.out.println(f);

		
//		*********************
		// 2. Non Primitive
		
		String str= "rounak";

	}

}
