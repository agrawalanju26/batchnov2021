
public class LoopsInJavas {

	public static void main(String[] args) {

//		1. Entry Control  (while, For)
//		2. Exit Control  (do while)

//		int a=2;
//		
//		while(a<=20) {
//			System.out.println(a);
//			a=a+2;
//		}

//		for(int i=2;i<=20;i=i+2) {
//			System.out.println(i);
//		}

		int j = 2;
		do {
			System.out.println(j);
			j=j+2;
		} while (j < 2);

	}
}
