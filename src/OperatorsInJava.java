
public class OperatorsInJava {

	public static void main(String[] args) {

		// Arithmetic operators
		// +,-,*,/,%
//		int a =41;
//		int b=20;
//		int c;
//		System.out.println(a+b);
//		System.out.println(a-b);
//		System.out.println(a*b);
//		c=a/b;
//		System.out.println(c);
//		c=a%b;
//		System.out.println(c);	

		// Relational Operator
		// <,><=,>=,==,!=
		int t=20;	
		System.out.println(t>0);
		System.out.println(t<19);
		System.out.println(t>=20);
		System.out.println(t<=20);
		System.out.println(t==20);

		// Assignment Operator
		// =, +=,-=,*=,/=,%=
		int u=34;
		u=u+2;
		u+=2;

		// Logical Operator
		// && , ||

//		int i=90;
//		int y=3;
//		System.out.println(i>0  && y>0);
//		System.out.println(i<0 && y>0);
//		
//		System.out.println(i<0 || y>0);

//		 Unary operator
//		 exp++,++exp, --exp, exp--

		int p = 1;
		int temp;
//		p++;
//		p++;
////		++p;
//		System.out.println(p);
//		temp = p++;  // temp = 1
		temp= ++p; // temp=2;
		System.out.println(temp);
		System.out.println(p);

	}
}
