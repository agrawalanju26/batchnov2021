
public class SwitchStatement {

	public static void main(String[] args) {
		int a = 21;

		switch (a) {

		case 1:
			System.out.println("Jan");
			break;
		case 2:
			System.out.println("feb");
			break;
		case 3:
			System.out.println("mar");
			break;
		default:
			System.out.println("it is not a valid month");
		}

	}

}
